import {Component, OnInit} from '@angular/core';
import {CustomFirstService} from '../custom-first/custom-first.service';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-custom-details',
  templateUrl: './custom-details.component.html',
  styleUrls: ['./custom-details.component.scss']
})
export class CustomDetailsComponent implements OnInit {

  public product: { id: number, name: string, description: string };

  constructor(private _customFirstService: CustomFirstService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    /*let id = this._activatedRoute.snapshot.paramMap.get('id');
    this.product = this._customFirstService.getById(+id);*/

    this._activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.product = this._customFirstService.getById(+id);
    });
  }

}
