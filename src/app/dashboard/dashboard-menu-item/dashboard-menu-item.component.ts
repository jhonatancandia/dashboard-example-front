import {Component, OnInit, Input} from '@angular/core';
import {DashboardMenuItem} from './dashboard-menu-item';

@Component({
  selector: 'app-dashboard-menu-item',
  templateUrl: './dashboard-menu-item.component.html',
  styleUrls: ['./dashboard-menu-item.component.scss']
})
export class DashboardMenuItemComponent implements OnInit {

  @Input() dashboardMenuItem: DashboardMenuItem;

  constructor() {
    this.dashboardMenuItem = new DashboardMenuItem('Custom First', '/custom-first');
  }

  ngOnInit() {
  }

}
