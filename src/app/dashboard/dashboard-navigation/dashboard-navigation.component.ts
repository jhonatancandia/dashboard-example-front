import {Component, OnInit} from '@angular/core';
import {DashboardMenuItem} from '../dashboard-menu-item/dashboard-menu-item';

@Component({
  selector: 'app-dashboard-navigation',
  templateUrl: './dashboard-navigation.component.html',
  styleUrls: ['./dashboard-navigation.component.scss']
})
export class DashboardNavigationComponent implements OnInit {

  public navigationItems: DashboardMenuItem[];

  constructor() {
    this.navigationItems = [];
  }

  ngOnInit() {
    this.navigationItems = [
      {
        label: 'Custom',
        route: '/custom'
      },
      {
        label: 'NG Bootstrap',
        route: '/ng-bootstrap'
      },{
        label: 'Prime NG',
        route: '/primeng'
      }
    ];
  }

}
