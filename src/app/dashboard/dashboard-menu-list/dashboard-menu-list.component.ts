import {Component, OnInit} from '@angular/core';
import {DashboardMenuItem} from '../dashboard-menu-item/dashboard-menu-item';
import {Router, Event, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-dashboard-menu-list',
  templateUrl: './dashboard-menu-list.component.html',
  styleUrls: ['./dashboard-menu-list.component.scss']
})
export class DashboardMenuListComponent implements OnInit {

  public menu: DashboardMenuItem[];

  constructor(private _router: Router) {
    this.menu = [];
  }

  ngOnInit() {
    this._routerListener();
  }

  private _routerListener(): void {
    this._router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this._showCorrespondingMenu();
      }
    });
  }

  private _showCorrespondingMenu(): void {
    /*switch (this._router.url) {
      case '/custom':
        this.menu = this._buildCustomMenu();
        break;
      case '/ng-bootstrap':
        this.menu = this._buildNgBootstrapMenu();
        break;
      default:
        break;
    }*/

    if (this._router.isActive('/custom', false)) {
      this.menu = this._buildCustomMenu();
    }

    if (this._router.isActive('/ng-bootstrap', false)) {
      this.menu = this._buildNgBootstrapMenu();
    }
    if (this._router.isActive('/primeng', false)) {
      this.menu = this._buildPrimeNgMenu();
    }
  }

  private _buildCustomMenu(): DashboardMenuItem[] {
    return [
      {
        label: 'Custom First',
        route: '/custom/first'
      },
      {
        label: 'Custom Second',
        route: '/custom/second'
      }
    ];
  }

  private _buildNgBootstrapMenu(): DashboardMenuItem[] {
    return [
      {
        label: 'NG Bootstrap 1',
        route: '/ng-bootstrap/first'
      }
    ];
  }

  private _buildPrimeNgMenu(): DashboardMenuItem[] {
    return [
      {
        label: 'Data',
        route: '/primeng/data'
      },
      {
        label: 'Lista',
        route: '/primeng/list'
      }
    ];
  }
}
