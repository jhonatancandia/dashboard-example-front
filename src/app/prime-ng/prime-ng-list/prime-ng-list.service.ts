import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrimeNgListService {

  private _students: { id:number, name:string, age:number }[];

  constructor() { 
    this._students = [
      {
        id: 1,
        name: 'Jhoanatan',
        age: 20
      },
      {
        id: 2,
        name: 'Juan',
        age: 23
      },
      {
        id: 3,
        name: 'Pedro',
        age: 25
      
      },
      {
        id: 4,
        name: 'Lucas',
        age: 26
      }
    ]
  }

  public getById(id: number): { id: number, name: string, age:number } {
    let response: { id: number, name: string, age:number } = null;

    for (let student of this._students) {
      if (student.id === id) {
        response = student;
      }
    }

    return response;
  }

  get students(): { id: number, name: string, age:number}[] {
    return this._students;
  }
}
