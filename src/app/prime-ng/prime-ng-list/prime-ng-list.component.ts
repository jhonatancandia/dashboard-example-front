import { Component, OnInit } from '@angular/core';
import { PrimeNgListService } from './prime-ng-list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-prime-ng-list',
  templateUrl: './prime-ng-list.component.html',
  styleUrls: ['./prime-ng-list.component.scss']
})
export class PrimeNgListComponent implements OnInit {

  public students:{ id:number, name:string, age:number }[];

  constructor(private _primgengListService: PrimeNgListService, private _router:Router) { 
    this.students = [];
  }

  ngOnInit() {
    this.students = this._primgengListService.students;
  }

  public navigate(id: number): void {
    this._router.navigate(['primeng', 'list', +id]);
  }
}
