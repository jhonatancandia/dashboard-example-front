import { TestBed } from '@angular/core/testing';

import { PrimeNgListService } from './prime-ng-list.service';

describe('PrimeNgListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrimeNgListService = TestBed.get(PrimeNgListService);
    expect(service).toBeTruthy();
  });
});
