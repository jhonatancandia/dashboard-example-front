import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-prime-ng-data',
  templateUrl: './prime-ng-data.component.html',
  styleUrls: ['./prime-ng-data.component.scss']
})
export class PrimeNgDataComponent implements OnInit {

  public title:string;
  public content:string;

  constructor(private _activatedRoute: ActivatedRoute) { 
    
  }

  ngOnInit() {
    this._activatedRoute.data.subscribe((data: { title: string, content: string }) => {
      this.title = data.title;
      this.content = data.content;
    });
  }

}
