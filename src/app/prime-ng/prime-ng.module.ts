import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNgRoutingModule } from './prime-ng-routing.module';
import { PrimeNgMainComponent } from './prime-ng-main/prime-ng-main.component';
import { PrimeNgDataComponent } from './prime-ng-data/prime-ng-data.component';
import { PrimeNgListComponent } from './prime-ng-list/prime-ng-list.component';
import { PrimeNgDetailComponent } from './prime-ng-detail/prime-ng-detail.component';

@NgModule({
  declarations: [PrimeNgMainComponent, PrimeNgDataComponent, PrimeNgListComponent, PrimeNgDetailComponent],
  imports: [
    CommonModule,
    PrimeNgRoutingModule
  ]
})
export class PrimeNgModule { }
