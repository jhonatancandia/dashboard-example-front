import {Routes} from '@angular/router';
import { PrimeNgMainComponent } from './prime-ng-main/prime-ng-main.component';
import { PrimeNgDataComponent } from './prime-ng-data/prime-ng-data.component';
import { PrimeNgListComponent } from './prime-ng-list/prime-ng-list.component';
import { PrimeNgDetailComponent } from './prime-ng-detail/prime-ng-detail.component';

export const PRIME_NG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimeNgMainComponent,
    children: [
      {
        path: '',
        redirectTo: '/primeng/data',
        pathMatch: 'full'
      },
      {
        path: 'data',
        data: {title: 'Laboratorio 3', content: 'This data was received from router and it is working'},
        component: PrimeNgDataComponent
      },
      {
        path: 'list',
        component: PrimeNgListComponent,
        children: [
          {
            path: ':id',
            component: PrimeNgDetailComponent
          }
        ]
      }
    ]
  }
];
