import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PrimeNgListService } from '../prime-ng-list/prime-ng-list.service';

@Component({
  selector: 'app-prime-ng-detail',
  templateUrl: './prime-ng-detail.component.html',
  styleUrls: ['./prime-ng-detail.component.scss']
})
export class PrimeNgDetailComponent implements OnInit {

  public student: { id:number, name:string, age:number };

  constructor(private _primgengListService: PrimeNgListService, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.student = this._primgengListService.getById(+id);
    });
  }

}
